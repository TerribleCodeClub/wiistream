#pragma once

#include <cstdint>
#include <memory>
#include <optional>
#include <string>
#include <string_view>
#include <network.h>


class Networking final
{
public:
    bool init() noexcept;
    static int32_t create_socket(uint8_t type);
    static std::unique_ptr<sockaddr_in> create_host(uint16_t port) noexcept;
    static std::unique_ptr<sockaddr_in> create_dest(std::string_view ip_address, uint16_t port) noexcept;
    std::optional<std::string> get_ip_address() noexcept;
    std::optional<std::string> get_netmask() noexcept;
    std::optional<std::string> get_gateway() noexcept;
private:
    static bool _connected;
    char _ip_address[16];
    char _netmask[16];
    char _gateway[16];
};
