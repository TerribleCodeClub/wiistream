#include "udp_client.h"

#include <string.h>
#include <network.h>

#include "networking.h"


UdpClient::UdpClient(std::string_view ip_address, uint16_t port):
    _sock { Networking::create_socket(SOCK_DGRAM) },
    _client { Networking::create_host(0) },
    _server { Networking::create_dest(ip_address, port) }
{
}

void UdpClient::send()
{
    net_sendto(_sock, "Ping!", sizeof("Ping!"), 0, (sockaddr *)(_server.get()), sizeof(sockaddr_in));
}

std::string UdpClient::receive()
{
    char buffer[1500] = {};
    net_recv(_sock, &buffer[0], 1500, 0);
    return std::string(buffer);
}