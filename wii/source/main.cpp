#include <cstdint>
#include <cstdlib>
#include <string>

#include <gccore.h>
#include <wiiuse/wpad.h>

#include "networking.h"
#include "screen.h"
#include "udp_client.h"

/// The IP address of the server to connect to.
const std::string SERVER_IP_ADDRESS = "192.168.0.1";

/// The port number of the server to connect to.
constexpr uint16_t SERVER_PORT = 10000;


int main(int argc, char **argv)
{
	// Screen init
	auto screen = Screen{};

	// Controller init
	WPAD_Init();

	// Network init
	Networking network;
	network.init();
	auto udp_client = UdpClient{ SERVER_IP_ADDRESS, SERVER_PORT };

	auto exit = false;
	while (!exit)
	{
		// Call WPAD_ScanPads each loop, this reads the latest controller states
		WPAD_ScanPads();
		const auto pressed = WPAD_ButtonsDown(0);
		if (pressed & WPAD_BUTTON_HOME)
			exit = true;

		// Clear the frame buffer.
		screen.clear();

		// drawing goes here

		// Wait for the next frame
		screen.wait_for_vsync();
	}

	return 0;
}
