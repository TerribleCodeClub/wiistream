#include "screen.h"

#include <gccore.h>

Screen::Screen()
{
    // Initialise the video system
    VIDEO_Init();

    // Obtain the preferred video mode from the system
	// This will correspond to the settings in the Wii menu
    _rmode = VIDEO_GetPreferredMode(NULL);

    // Allocate memory for the display in the uncached region
    _xfb = MEM_K0_TO_K1(SYS_AllocateFramebuffer(_rmode));

    // Set up the video registers with the chosen mode
	VIDEO_Configure(_rmode);

	// Tell the video hardware where our display memory is
	VIDEO_SetNextFramebuffer(_xfb);

	// Make the display visible
	VIDEO_SetBlack(FALSE);

	// Flush the video register changes to the hardware
	VIDEO_Flush();

	// Wait for Video setup to complete
	VIDEO_WaitVSync();
	if (_rmode->viTVMode & VI_NON_INTERLACE)
		VIDEO_WaitVSync();
}

void Screen::clear() noexcept
{
	VIDEO_ClearFrameBuffer(_rmode, _xfb, COLOR_BLACK);
}

void Screen::draw(int x, int y, uint16_t colour) noexcept
{
	reinterpret_cast<uint16_t*>(_xfb)[(y * _rmode->fbWidth) + x] = colour;
}

void Screen::wait_for_vsync() const noexcept
{
	VIDEO_WaitVSync();
}

int Screen::width() const noexcept
{
	return _rmode->fbWidth;
}

int Screen::height() const noexcept
{
	return _rmode->xfbHeight;
}