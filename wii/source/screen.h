#pragma once

#include <cstdint>

#include <gccore.h>


/// Class for drawing to the screen, simple abstraction around the raw
/// framebuffer.
class Screen final
{
public:
    /// Constructor.
    ///
    /// \remarks Automatically selects the display settings.
    Screen();

    /// Clear the screen.
    void clear() noexcept;

    /// Draw a pixel to the screen.
    ///
    /// \param x The x-coordinate to draw to.
    /// \param y The y-coordinate to draw to.
    /// \param colour The colour value to draw to the coordinate.
    void draw(int x, int y, uint16_t colour) noexcept;

    /// Wait for a v-sync.
    void wait_for_vsync() const noexcept;

    /// \return The width of the screen in pixels.
    int width() const noexcept;

    /// \return The height of the screen in pixels.
    int height() const noexcept;

private:
    /// The allocated framebuffer.
    void* _xfb;

    /// The rendermode object.
    GXRModeObj* _rmode;
};