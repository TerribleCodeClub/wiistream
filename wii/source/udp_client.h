#pragma once

#include <cstdint>
#include <memory>
#include <string>
#include <string_view>
#include <network.h>


class UdpClient final
{
public:
    UdpClient(std::string_view ip_address, uint16_t port);
    void send();
    std::string receive();
private:
    int32_t _sock;
    std::unique_ptr<sockaddr_in> _client;
    std::unique_ptr<sockaddr_in> _server;
};
