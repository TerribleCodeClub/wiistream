import socket


class Networking:

    MAX_PACKET_SIZE = 512

    def __init__(self, ip, port) -> None:
        if port < 1 or port > 65535:
            raise ValueError("Fuck off bad port number")

        # also validate ip?

        self._socket = socket.socket(socket.AF_INET, socket.SOCK_DGRAM)
        self._socket.bind((ip, port))
        self._targets = []

    def add_target(self, target):
        self._targets.append(target)

    def send_buffer(self, to_send):
        chunks = [
            to_send[i : i + Networking.MAX_PACKET_SIZE]
            for i in range(0, len(to_send), Networking.MAX_PACKET_SIZE)
        ]
        for chunk in chunks:
            for target in self._targets:
                self._socket.sendto(chunk, target)
