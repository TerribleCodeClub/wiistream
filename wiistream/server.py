import pygame
import logging
import os
import argparse


from wiistream.pong.pong import Pong
from wiistream.networking import Networking

SIZE_X = 640
SIZE_Y = 480


def parse_args():
    """ """
    parser = argparse.ArgumentParser(description="Dumb wii multiplayer project.")
    parser.add_argument(
        "--debug", action="store_true", help="Print all the log messages"
    )
    args = parser.parse_args()
    return args


def setup_logging(debug):
    """

    :return:
    """
    logging.basicConfig()
    if debug:
        logging.getLogger().setLevel(logging.INFO)


def setup_pygame():
    """

    :return:
    """
    os.environ["SDL_VIDEODRIVER"] = "dummy"
    pygame.init()

    screen = pygame.display.set_mode((SIZE_X, SIZE_Y))
    clock = pygame.time.Clock()
    return screen, clock


def run_loop(screen, clock, network):
    """

    :return:
    """
    game = Pong(screen, SIZE_X, SIZE_Y)

    while True:
        # check input

        # update game state
        game.update_state()

        # call draw function
        game.draw_screen()

        # fire off frames

        # wait!
        clock.tick(60)

        network.send_buffer(pygame.image.tostring(screen, "RGB"))


def main():
    args = parse_args()
    setup_logging(args.debug)
    # setup networking
    try:
        network = Networking("127.0.0.1", 6969)
    except (ValueError, OSError) as e:
        logging.error(f"Error setting up networking: {e}")
        exit(-1)
    
    network.add_target(("127.0.0.1", 420))

    screen, clock = setup_pygame()
    try:
        run_loop(screen, clock, network)
    except (KeyboardInterrupt, SystemExit):
        logging.info("Stopping...")
    except Exception as e:
        logging.error(f"Top level exception, exiting: {e}")
    pygame.quit()
    print("ok!")
