import pygame
from random import randint

from wiistream.helper import flip_coin

BLACK = (0, 0, 0)


class Ball(pygame.sprite.Sprite):
    # This class represents a car. It derives from the "Sprite" class in Pygame.

    def __init__(self, color, width, height, x_pos, y_pos):
        # Call the parent class (Sprite) constructor
        super().__init__()

        # Pass in the color of the car, and its x and y position, width and height.
        # Set the background color and set it to be transparent
        self.image = pygame.Surface([width, height])
        self.image.fill(BLACK)
        self.image.set_colorkey(BLACK)

        self._startX = x_pos
        self._startY = y_pos

        # Draw the ball (a rectangle!)
        pygame.draw.rect(self.image, color, [0, 0, width, height])

        self._velocity = [randint(4, 8), randint(-8, 8)]
        if flip_coin():
            self._velocity[0] = -self._velocity[0]

        # Fetch the rectangle object that has the dimensions of the image.
        self.rect = self.image.get_rect()
        self.rect.x = x_pos
        self.rect.y = y_pos

    def reset(self):
        self._velocity = [randint(4, 6), randint(-8, 8)]
        if flip_coin():
            self._velocity[0] = -self._velocity[0]
        self.rect.x = self._startX
        self.rect.y = self._startY

    @property
    def x(self):
        return self.rect.x

    @property
    def y(self):
        return self.rect.y

    @property
    def velocity(self):
        return self._velocity

    def update(self):
        self.rect.x += self._velocity[0]
        self.rect.y += self._velocity[1]

    def bounce(self):
        self._velocity[0] = -self._velocity[0]
        # sometimes speed up
        if flip_coin():
            if self._velocity[0] < 0:
                self._velocity[0] -= 1
            else:
                self._velocity[0] += 1
        self._velocity[1] = randint(-8, 8)
