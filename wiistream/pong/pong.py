# Import the pygame library and initialise the game engine
import pygame

from wiistream.pong.paddle import Paddle
from wiistream.pong.ball import Ball

# Define some colors
BLACK = (0, 0, 0)
WHITE = (255, 255, 255)


class Pong:
    def __init__(self, game_screen, x, y):
        """

        :param game_screen:
        :param x:
        :param y:
        """
        self._screen = game_screen
        self._width = x
        self._height = y

        # Initialise player scores
        self._scorePlayerL = 0
        self._scorePlayerR = 0

        self._update = True

        self._paddleL = Paddle(WHITE, 10, 100, 20, 200)

        self._paddleR = Paddle(WHITE, 10, 100, self._width - 30, 200)

        # minus 5 since pygame draws from the top left, get it centered exactly
        self._ball = Ball(WHITE, 10, 10, (self._width / 2) - 5, (self._height / 2) - 5)

        # This will be a list that will contain all the sprites we intend to use in our game.
        self._all_sprites_list = pygame.sprite.Group()

        # Add the car to the list of objects
        self._all_sprites_list.add(self._paddleL)
        self._all_sprites_list.add(self._paddleR)
        self._all_sprites_list.add(self._ball)

        self._update_event = pygame.event.custom_type()

    # # Moving the paddles when the use uses the arrow keys (player A) or "W/S" keys (player B)
    # keys = pygame.key.get_pressed()
    # if keys[pygame.K_w]:
    #     paddleA.moveUp(5)
    # if keys[pygame.K_s]:
    #     paddleA.moveDown(5)
    # if keys[pygame.K_UP]:
    #     paddleB.moveUp(5)
    # if keys[pygame.K_DOWN]:
    #     paddleB.moveDown(5)

    def update_state(self):
        # delete this
        for event in pygame.event.get():
            if event.type == self._update_event:
                self._update = True

        if not self._update:
            return

        self._all_sprites_list.update()

        score = False

        # Check if the ball is bouncing against any of the 4 walls:
        if self._ball.x >= self._width - 5:
            self._scorePlayerL += 1
            score = True
        if self._ball.x <= 0:
            self._scorePlayerR += 1
            score = True
        if self._ball.rect.y > self._height - 5:
            self._ball.velocity[1] = -self._ball.velocity[1]
        if self._ball.rect.y < 0:
            self._ball.velocity[1] = -self._ball.velocity[1]

        if score:
            self._ball.reset()
            self._update = False
            pygame.time.set_timer(self._update_event, 500)
            return

            # Detect collisions between the ball and the paddles
        if pygame.sprite.collide_mask(
            self._ball, self._paddleL
        ) or pygame.sprite.collide_mask(self._ball, self._paddleR):
            self._ball.bounce()

    def draw_screen(self):
        # --- Drawing code should go here
        # First, clear the screen to black.
        self._screen.fill(BLACK)
        # Draw the net
        pygame.draw.line(
            self._screen,
            WHITE,
            [(self._width / 2) - 2, 0],
            [(self._width / 2) - 2, self._height],
            4,
        )

        # Now let's draw all the sprites in one go. (For now we only have 2 sprites!)
        self._all_sprites_list.draw(self._screen)

        # Display scores:
        font = pygame.font.Font(None, 74)
        text = font.render(str(self._scorePlayerL), True, WHITE)
        self._screen.blit(text, (self._width / 4, 10))
        text = font.render(str(self._scorePlayerR), True, WHITE)
        self._screen.blit(text, ((self._width / 4) * 3, 10))

        # --- Go ahead and update the screen with what we've drawn.
        pygame.display.flip()
